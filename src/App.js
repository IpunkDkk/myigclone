/* eslint-disable no-trailing-spaces */
/* eslint-disable prettier/prettier */
import React , {useState} from 'react';
import {View, ScrollView} from 'react-native';
import Header from './components/header';
import Content from './components/content';

const App = () => {
  const [klik, tambahklik] = useState(0);
  return (
    <ScrollView>
    <View>
      <Header notif={klik} reset={() => tambahklik(0)}  />
      <Content tambah={() => tambahklik(klik+1)} />
      <Content tambah={() => tambahklik(klik+1)} />
      <Content tambah={() => tambahklik(klik+1)} />
      <Content tambah={() => tambahklik(klik+1)} />
      <Content tambah={() => tambahklik(klik+1)} />
      <Content tambah={() => tambahklik(klik+1)} />
      
    </View>
    </ScrollView>
  );
};


export default App;
